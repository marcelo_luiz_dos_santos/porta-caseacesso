package br.com.itau.PortaCaseAcesso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class PortaCaseAcessoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PortaCaseAcessoApplication.class, args);
	}

}
