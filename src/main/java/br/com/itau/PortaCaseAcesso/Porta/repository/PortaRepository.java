package br.com.itau.PortaCaseAcesso.Porta.repository;

import br.com.itau.PortaCaseAcesso.Porta.model.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository <Porta, Integer> {
}
