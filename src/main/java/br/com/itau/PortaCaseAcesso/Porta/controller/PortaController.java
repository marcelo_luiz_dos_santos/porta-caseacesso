package br.com.itau.PortaCaseAcesso.Porta.controller;

import br.com.itau.PortaCaseAcesso.Porta.model.Porta;
import br.com.itau.PortaCaseAcesso.Porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.sound.sampled.Port;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta criarPorta (@RequestBody @Valid Porta porta){
        Porta portaObjeto = portaService.criarPorta(porta);
        return portaObjeto;
    }

    @GetMapping("/{id}")
    public Optional<Porta> buscarPorId (@PathVariable(name = "id") int id){
        try{
            Optional<Porta> porta = portaService.buscarPorId(id);
            return porta;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }
}
