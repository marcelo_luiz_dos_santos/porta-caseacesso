package br.com.itau.PortaCaseAcesso.Porta.service;

import br.com.itau.PortaCaseAcesso.Porta.model.Porta;
import br.com.itau.PortaCaseAcesso.Porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class PortaService {
    @Autowired
    private PortaRepository portaRepository;

    public Porta criarPorta(Porta porta){
        Porta portaObjeto = portaRepository.save(porta);
        return portaObjeto;
    }

    public Optional<Porta> buscarPorId (int id){
        Optional<Porta> optionalPorta = portaRepository.findById(id);
        if (optionalPorta.isPresent()){
            return optionalPorta;
        }
        throw new RuntimeException("A porta de acesso não foi encontrada.");
    }
}
